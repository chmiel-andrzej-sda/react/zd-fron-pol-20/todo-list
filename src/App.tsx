import React from 'react';
import './App.scss';
import { ToDo } from './entity/ToDo';
import { ToDoList } from './component/ToDoList';
import { CreateToDoComponent } from './component/CreateToDoComponent';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { HalfDynamicInput } from './component/HalfDynamicInput';

export function App(): JSX.Element {
	const [toDos, setToDos] = React.useState<ToDo[]>([
		{
			content: 'test 1',
			createDate: new Date()
		},
		{
			content: 'test 2',
			createDate: new Date()
		}
	]);

	function handleDoneClick(toDo: ToDo): void {
		const found: number = toDos.findIndex((td: ToDo): boolean => td === toDo);
		toDos[found] = {
			...toDos[found],
			doneDate: new Date()
		};
		setToDos([...toDos]);
	}

	function handleUndoneClick(toDo: ToDo): void {
		const found: number = toDos.findIndex((td: ToDo): boolean => td === toDo);
		toDos[found] = {
			...toDos[found],
			doneDate: undefined
		};
		setToDos([...toDos]);
	}

	function handleCreate(toDo: ToDo): void {
		setToDos([...toDos, toDo]);
	}

	return (
		<div className='App'>
			<h1>ToDo List</h1>
			<BrowserRouter>
				<Link to='list'>List</Link>
				<Link to='create'>Create!</Link>
				<Routes>
					<Route
						path='list'
						element={
							<ToDoList
								toDos={toDos}
								onDoneClick={handleDoneClick}
								onUndoneClick={handleUndoneClick}
							/>
						}
					/>
					<Route
						path='create/:text?'
						element={<CreateToDoComponent onCreate={handleCreate} />}
					/>
					<Route path='*' element={<div>Not Found</div>} />
				</Routes>
			</BrowserRouter>
			<HalfDynamicInput value='test' />
		</div>
	);
}
