export interface ToDo {
	readonly content: string;
	readonly createDate: Date;
	readonly deadline?: Date;
	readonly doneDate?: Date;
}

export interface GenericType<T extends string | number | Date = number> {
	id: number;
	value: T;
	values: T[];
	func: (t: T) => void;
}

export const g: GenericType<string> = {
	id: 10,
	value: 's',
	values: ['a', 'b', 'c'],
	func: (s: string) => undefined
};

export const o: GenericType<number> = {
	id: 10,
	value: 7,
	values: [1, 2, 3],
	func: (s: number) => undefined
};

export const p: GenericType = {
	id: 10,
	value: 7,
	values: [1, 2, 3],
	func: (s: number) => undefined
};

export function xyz<T>(count: number, f: () => T): T[] {
	const result: T[] = [];
	for (let i = 0; i < count; i++) {
		result.push(f());
	}
	return result;
}

const x = xyz<string>(10, () => '10');
