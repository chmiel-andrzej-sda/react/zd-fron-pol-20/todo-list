import React from 'react';
import './ToDoList.scss';
import { ToDo } from '../entity/ToDo';
import { ToDoComponent } from './ToDoComponent';

interface ToDoListProps {
	readonly toDos: ToDo[];
	readonly onDoneClick: (toDo: ToDo) => void;
	readonly onUndoneClick: (toDo: ToDo) => void;
}

export function ToDoList(props: ToDoListProps): JSX.Element {
	function renderToDo(toDo: ToDo, index: number): JSX.Element {
		return (
			<ToDoComponent
				key={index}
				toDo={toDo}
				onDoneClick={(): void => props.onDoneClick(toDo)}
				onUndoneClick={(): void => props.onUndoneClick(toDo)}
			/>
		);
	}

	function compareToDos(a: ToDo, b: ToDo): number {
		if (a.doneDate !== undefined && b.doneDate === undefined) {
			return 1;
		}
		if (a.doneDate === undefined && b.doneDate !== undefined) {
			return -1;
		}
		return 0;
	}

	return (
		<div className='ToDoList'>
			<span className='title'>ToDoList</span>
			{props.toDos.sort(compareToDos).map(renderToDo)}
		</div>
	);
}
