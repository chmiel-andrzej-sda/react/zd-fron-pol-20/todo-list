import React from 'react';
import './ToDoComponent.scss';
import { ToDo } from '../entity/ToDo';
import { Button } from './Button';

interface ToDoComponentProps {
	readonly toDo: ToDo;
	readonly onDoneClick: () => void;
	readonly onUndoneClick: () => void;
}

export function ToDoComponent(props: ToDoComponentProps): JSX.Element {
	function getContentClassName(): string | undefined {
		if (props.toDo.doneDate) {
			return 'done';
		}
		return undefined;
	}

	function getDeadlineClassName(): string | undefined {
		if (props.toDo.deadline && new Date() >= props.toDo.deadline) {
			return 'outdated';
		}
		return undefined;
	}

	function renderButton(): JSX.Element {
		if (props.toDo.doneDate) {
			return <Button onClick={props.onUndoneClick}>UNDONE</Button>;
		}
		return <Button onClick={props.onDoneClick}>DONE</Button>;
	}

	return (
		<div>
			<span className={getContentClassName()}>{props.toDo.content}</span>|
			<span>{props.toDo.createDate.toISOString()}</span>|
			<span className={getDeadlineClassName()}>
				{props.toDo.deadline?.toISOString()}
			</span>
			|<span>[{props.toDo.doneDate?.toISOString()}]</span>
			{renderButton()}
		</div>
	);
}
