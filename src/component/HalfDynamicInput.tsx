import React from 'react';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';

interface HalfDynamicInputProps {
	value?: string;
}

export function HalfDynamicInput(props: HalfDynamicInputProps): JSX.Element {
	const [dynamically, setDynamically] = React.useState<boolean>(false);
	const [value, setValue] = React.useState<string>(props.value || '');
	const [realValue, setRealValue] = React.useState<string>(props.value || '');
	const [error, setError] = React.useState<boolean>(false);

	function handleClick(): void {
		setDynamically((prev: boolean): boolean => !prev);
		setRealValue(value);
		setError(false);
	}

	function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setValue(event.currentTarget.value);
		if (dynamically) {
			setRealValue(event.currentTarget.value);
		}
		setError(false);
	}

	function handleGoClick(): void {
		if (value) {
			setRealValue(value);
		} else {
			setError(true);
		}
	}

	return (
		<div>
			<Form>
				<InputGroup className='mb-3'>
					<Form.Control
						value={value}
						placeholder='Type something'
						onChange={handleChange}
						isInvalid={error}
					/>
					{!dynamically && (
						<Button variant='success' onClick={handleGoClick}>
							Go
						</Button>
					)}
					<Button
						variant={dynamically ? 'primary' : 'outline-primary'}
						onClick={handleClick}
					>
						Dynamically
					</Button>
				</InputGroup>
			</Form>
			{realValue}
		</div>
	);
}
