import React from 'react';
import { ToDo } from '../entity/ToDo';
import { Button } from './Button';
import { useParams } from 'react-router-dom';

interface CreateToDoComponentProps {
	readonly onCreate: (toDo: ToDo) => void;
}

type RouteParams = {
	text?: string;
};

export function CreateToDoComponent(
	props: CreateToDoComponentProps
): JSX.Element {
	const params: RouteParams = useParams<RouteParams>();
	const [content, setContent] = React.useState<string>(params.text || '');
	const [deadline, setDeadline] = React.useState<Date | undefined>(undefined);

	function handleChange(event: React.ChangeEvent<HTMLTextAreaElement>): void {
		setContent(event.currentTarget.value);
	}

	function handleDateChange(event: React.ChangeEvent<HTMLInputElement>): void {
		try {
			const date: Date = new Date(event.currentTarget.value);
			date.toISOString();
			setDeadline(date);
		} catch (error) {
			setDeadline(undefined);
		}
	}

	function handleClick(): void {
		if (content) {
			setContent('');
			props.onCreate({
				createDate: new Date(),
				content,
				deadline
			});
		}
	}

	return (
		<div>
			<textarea value={content} onChange={handleChange} />
			<br />
			<input
				type='date'
				value={deadline?.toISOString().slice(0, 10)}
				min={new Date().toISOString().slice(0, 10)}
				onChange={handleDateChange}
			/>
			<Button onClick={handleClick} disabled={content === ''}>
				ADD
			</Button>
		</div>
	);
}
