import React from 'react';
import {
	ReactTestInstance,
	ReactTestRenderer,
	create
} from 'react-test-renderer';
import { ToDoList } from '../../src/component/ToDoList';
import {
	toDoWithFullData,
	toDoWithoutFullData
} from '../provider/TestDataProvider';

describe('ToDoList', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(
			<ToDoList
				onDoneClick={(): void => undefined}
				onUndoneClick={(): void => undefined}
				toDos={[
					toDoWithFullData,
					toDoWithoutFullData,
					toDoWithoutFullData,
					toDoWithFullData
				]}
			/>
		);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('handles done click', (): void => {
		// given
		const clickMock: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(
			<ToDoList
				onDoneClick={clickMock}
				onUndoneClick={(): void => undefined}
				toDos={[toDoWithoutFullData]}
			/>
		);

		// when
		const button: ReactTestInstance = component.root.findByType('button');
		button.props.onClick();

		// then
		expect(clickMock).toHaveBeenCalledTimes(1);
		expect(clickMock).toHaveBeenCalledWith(toDoWithoutFullData);
	});

	it('handles undone click', (): void => {
		// given
		const clickMock: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(
			<ToDoList
				onDoneClick={(): void => undefined}
				onUndoneClick={clickMock}
				toDos={[toDoWithFullData]}
			/>
		);

		// when
		const button: ReactTestInstance = component.root.findByType('button');
		button.props.onClick();

		// then
		expect(clickMock).toHaveBeenCalledTimes(1);
		expect(clickMock).toHaveBeenCalledWith(toDoWithFullData);
	});
});
