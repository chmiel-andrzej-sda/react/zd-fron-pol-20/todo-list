import React from 'react';
import {
	ReactTestInstance,
	ReactTestRenderer,
	create,
	act
} from 'react-test-renderer';
import { HalfDynamicInput } from '../../src/component/HalfDynamicInput';

describe('HalfDynamicInput', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(<HalfDynamicInput />);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('renders with default', (): void => {
		// when
		const component: ReactTestRenderer = create(
			<HalfDynamicInput value='default value' />
		);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('changes input', (): void => {
		// given
		const component: ReactTestRenderer = create(<HalfDynamicInput />);

		// when
		const input: ReactTestInstance = component.root.findByType('input');
		act((): void => {
			input.props.onChange({ currentTarget: { value: 'test' } });
		});

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('changes input and clicks go', (): void => {
		// given
		const component: ReactTestRenderer = create(<HalfDynamicInput />);

		// when
		const input: ReactTestInstance = component.root.findByType('input');
		const button: ReactTestInstance = component.root.findByProps({
			children: 'Go'
		});
		act((): void => {
			input.props.onChange({ currentTarget: { value: 'test' } });
		});
		act((): void => {
			button.props.onClick();
		});

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('clicks go with no value', (): void => {
		// given
		const component: ReactTestRenderer = create(<HalfDynamicInput />);

		// when
		const button: ReactTestInstance = component.root.findByProps({
			children: 'Go'
		});
		act((): void => {
			button.props.onClick();
		});

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('removes error marker with input change', (): void => {
		// given
		const component: ReactTestRenderer = create(<HalfDynamicInput />);

		// when
		const button: ReactTestInstance = component.root.findByProps({
			children: 'Go'
		});
		const input: ReactTestInstance = component.root.findByType('input');
		act((): void => {
			button.props.onClick();
		});
		act((): void => {
			input.props.onChange({ currentTarget: { value: 'test' } });
		});

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('changes dynamic state', (): void => {
		// given
		const component: ReactTestRenderer = create(<HalfDynamicInput />);

		// when
		const button: ReactTestInstance = component.root.findByProps({
			children: 'Dynamically'
		});
		act((): void => {
			button.props.onClick();
		});

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});
});
