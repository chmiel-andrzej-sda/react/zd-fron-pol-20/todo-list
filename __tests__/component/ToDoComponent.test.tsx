import React from 'react';
import {
	ReactTestInstance,
	ReactTestRenderer,
	create
} from 'react-test-renderer';
import { ToDoComponent } from '../../src/component/ToDoComponent';
import {
	toDoWithFullData,
	toDoWithoutFullData
} from '../provider/TestDataProvider';

describe('ToDoComponent', (): void => {
	it('renders with full data', (): void => {
		// when
		const component: ReactTestRenderer = create(
			<ToDoComponent
				onDoneClick={(): void => undefined}
				onUndoneClick={(): void => undefined}
				toDo={toDoWithFullData}
			/>
		);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('renders without full data', (): void => {
		// when
		const component: ReactTestRenderer = create(
			<ToDoComponent
				onDoneClick={(): void => undefined}
				onUndoneClick={(): void => undefined}
				toDo={toDoWithoutFullData}
			/>
		);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('handles done click', (): void => {
		// given
		const clickMock: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(
			<ToDoComponent
				onDoneClick={clickMock}
				onUndoneClick={(): void => undefined}
				toDo={toDoWithoutFullData}
			/>
		);

		// when
		const button: ReactTestInstance = component.root.findByType('button');
		button.props.onClick();

		// then
		expect(clickMock).toHaveBeenCalledTimes(1);
	});

	it('handles undone click', (): void => {
		// given
		const clickMock: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(
			<ToDoComponent
				onDoneClick={(): void => undefined}
				onUndoneClick={clickMock}
				toDo={toDoWithFullData}
			/>
		);

		// when
		const button: ReactTestInstance = component.root.findByType('button');
		button.props.onClick();

		// then
		expect(clickMock).toHaveBeenCalledTimes(1);
	});
});
