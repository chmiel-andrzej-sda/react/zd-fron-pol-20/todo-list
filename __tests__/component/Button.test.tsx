import React from 'react';
import {
	ReactTestInstance,
	ReactTestRenderer,
	create
} from 'react-test-renderer';
import { Button } from '../../src/component/Button';

describe('Button', (): void => {
	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(
			<Button onClick={(): void => undefined}>Test</Button>
		);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('handles click', (): void => {
		// given
		const clickMock: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(
			<Button onClick={clickMock}>Test</Button>
		);

		// when
		const button: ReactTestInstance = component.root.findByType('button');
		button.props.onClick();

		// then
		expect(clickMock).toHaveBeenCalledTimes(1);
	});
});
