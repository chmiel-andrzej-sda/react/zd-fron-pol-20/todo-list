import React from 'react';
import {
	ReactTestInstance,
	ReactTestRenderer,
	create,
	act
} from 'react-test-renderer';
import { CreateToDoComponent } from '../../src/component/CreateToDoComponent';

describe('CreateToDoComponent', (): void => {
	beforeAll((): void => {
		jest.useFakeTimers('modern');
		jest.setSystemTime(new Date(Date.UTC(2020, 3, 1)));
	});

	afterAll((): void => {
		jest.useRealTimers();
	});

	it('renders', (): void => {
		// when
		const component: ReactTestRenderer = create(
			<CreateToDoComponent onCreate={(): void => undefined} />
		);

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('changes state with invalid date', (): void => {
		// given
		const component: ReactTestRenderer = create(
			<CreateToDoComponent onCreate={(): void => undefined} />
		);

		// when
		const textarea: ReactTestInstance = component.root.findByType('textarea');
		const input: ReactTestInstance = component.root.findByType('input');
		act(() => {
			textarea.props.onChange({ currentTarget: { value: 'test text' } });
			input.props.onChange({ currentTarget: { value: 'test text' } });
		});

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('changes state with valid date', (): void => {
		// given
		const component: ReactTestRenderer = create(
			<CreateToDoComponent onCreate={(): void => undefined} />
		);

		// when
		const textarea: ReactTestInstance = component.root.findByType('textarea');
		const input: ReactTestInstance = component.root.findByType('input');
		act(() => {
			textarea.props.onChange({ currentTarget: { value: 'test text' } });
			input.props.onChange({ currentTarget: { value: '2023-01-01' } });
		});

		// then
		expect(component.toJSON()).toMatchSnapshot();
	});

	it('handles create with content', (): void => {
		// given
		const createMock: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(
			<CreateToDoComponent onCreate={createMock} />
		);

		// when
		const textarea: ReactTestInstance = component.root.findByType('textarea');
		const input: ReactTestInstance = component.root.findByType('input');
		const button: ReactTestInstance = component.root.findByType('button');
		act((): void => {
			textarea.props.onChange({ currentTarget: { value: 'test text' } });
			input.props.onChange({ currentTarget: { value: '2023-01-01' } });
		});

		act((): void => {
			button.props.onClick();
		});

		act((): void => {
			// then
			expect(createMock).toHaveBeenCalledTimes(1);
			expect(createMock).toHaveBeenCalledWith({
				createDate: new Date(),
				content: 'test text',
				deadline: new Date(Date.UTC(2023, 0, 1))
			});
			expect(component.toJSON()).toMatchSnapshot();
		});
	});

	it('handles create without content', (): void => {
		// given
		const createMock: jest.Mock = jest.fn();
		const component: ReactTestRenderer = create(
			<CreateToDoComponent onCreate={createMock} />
		);

		// when
		const button: ReactTestInstance = component.root.findByType('button');
		button.props.onClick();

		// then
		expect(createMock).toHaveBeenCalledTimes(0);
	});
});
