import { ToDo } from '../../src/entity/ToDo';

export const toDoWithoutFullData: ToDo = {
	content: 'Test',
	createDate: new Date(Date.UTC(2023, 5, 14, 12, 34, 56))
};

export const toDoWithFullData: ToDo = {
	...toDoWithoutFullData,
	doneDate: new Date(Date.UTC(2023, 5, 14, 12, 34, 56)),
	deadline: new Date(Date.UTC(2000, 12, 3, 1, 2, 3))
};
